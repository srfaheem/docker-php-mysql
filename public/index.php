<?php
try {
    $conn = new PDO("mysql:host=database;dbname=dockerApp", "root", "secret");
    echo "Connected successfully.";
} catch (PDOException $pe) {
    die("Could not connect to the database" . $pe->getMessage());
}
